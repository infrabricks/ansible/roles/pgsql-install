# PGSQL install
Ansible role to install PostgreSQL. Use official PostgreSQL org repos.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name             | Variable description         | Type    | Default | Required
---                       | ---                          | ---     | ---     | ---
pgsql_local_repokeys_path | Local path of repos pubkeys  | string  | Yes     | Yes
pgsql_pgsl_repo_url       | Remote Postgresql repo URL   | string  | Yes     | Yes
pgsql_pgsl_repo_pubkey    | Postgresql repo pubkey name  | string  | Yes     | Yes
pgsql_pgsl_repo_keyring   | Postgresql repo keyring name | string  | Yes     | Yes
pgsql_version             | PostgreSQL version           | string  | Yes     | Yes
pgsql_packages            | Packages list to install     | string  | Yes     | Yes

## Dependencies

* No

## Playbook Example

```
---
- name: Configure and install PostgreSQL
  hosts: all
  become: yes
  vars_files:
    - vars/main.yml
  roles:
    - pgsql_install
```

## License

GPL v3

## Links

* [PostgreSQL](https://www.postgresql.org/)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
